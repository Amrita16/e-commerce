import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../common/services/auth.service';
import { ILogin } from "../common/interface";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {
  loginForm:FormGroup;

  constructor(private fb:FormBuilder, private loginService:AuthService,private router:Router) {}

  ngOnInit(){
      this.createForm();
  }
  createForm(){
      this.loginForm = this.fb.group({
        email:['',(Validators.required,Validators.email)],
        password:['',Validators.required]
      });
  }

  public login(){
    if(this.loginForm.valid){
      let formData:ILogin = this.loginForm.value;
      this.loginService.login(formData);
    }else{
      console.log(`Please enter all the fields.`);
    }
  }

  ngOnDestroy(){}
}
