import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { CarouselComponent } from '../common/carousel/carousel.component';
import { DealListComponent } from '../common/deal-list/deal-list.component';

@NgModule({
  declarations: [
    HomeComponent,
    CarouselComponent,
    DealListComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    HomeComponent,
    DealListComponent
  ]
})
export class HomeModule { }
