import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deal-list',
  templateUrl: './deal-list.component.html',
  styleUrls: ['./deal-list.component.css']
})
export class DealListComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  public goToProducts(){
    this.router.navigate(['products']);
  }
}
