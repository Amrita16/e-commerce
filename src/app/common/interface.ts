export interface ISignUp{
    firstname:string,
    lastname:string,
    email:string,
    password:string
}

export interface ILogin{
    email:string,
    password:string
}