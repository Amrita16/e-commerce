import { Injectable } from "@angular/core";
import { Config, BaseService } from "./base.service";
import { HttpClient } from "@angular/common/http";
import { RouteConstants } from "../constants/routes.constants";
import { Router } from "@angular/router";
import { ISignUp, ILogin } from "../interface";

@Injectable({providedIn:'root'})
export class AuthService extends BaseService{
    timer: any;
    // authStatus:boolean=false;
    token: string;

    constructor(private http:HttpClient,private router:Router){
        super();
    }
    // getAuthStatus(){
    //     return this.authStatus;
    // }
    getAuthToken(){
        return this.token;
    }
    signUp(reqObj:ISignUp){
        return this.http.post<Config>(`${this.getBaseUrl()}${RouteConstants.signup}`,reqObj ,this.getConfig());
    }
    login(reqObj:ILogin){
        this.http.post<Config>(`${this.getBaseUrl()}${RouteConstants.login}`,reqObj,this.getConfig()).subscribe(response=>{
            if(response.status){
            //   this.authStatus=true;
              this.token = response.token;
              this.setAuthData(response.token,response.expiresIn);
              this.setTimer(response.expiresIn);
              this.router.navigate(['/']);
            }else console.log(response.error);
          });
    }
    setAuthData(token:string,expirationTime:any){
        localStorage.setItem('authToken',token);
        localStorage.setItem('expirationTime',expirationTime);
        localStorage.setItem('authStatus','true');
    }
    private clearAuthData(){
        localStorage.removeItem('authToken');
        localStorage.removeItem('expirationTime');
        localStorage.removeItem('authStatus');
    }
    private setTimer(expirationTime:any){
        this.timer = setTimeout(()=>{
            this.logOut();
        },expirationTime*1000);
    }
    logOut(){
        this.clearAuthData();
        clearTimeout(this.timer);
        // this.authStatus=false;
        this.router.navigate(['/login']);
    }
}