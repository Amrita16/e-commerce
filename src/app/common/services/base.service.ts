import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

export interface Config{
    status: boolean,
    message?: string,
    token?:string,
    data?:any,
    error?:string,
    expiresIn?:number
}

@Injectable()
export class BaseService{
    constructor(){}

    public getBaseUrl(){
        const baseUrl = 'http://localhost:3000/';
        return baseUrl;
    }

    public getConfig(){
        let headers: HttpHeaders;
        headers = new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
        });
        return {
            headers:headers
        };
    }
}