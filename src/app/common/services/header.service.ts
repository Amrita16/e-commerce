import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BaseService } from "./base.service";
import {Config} from "../services/base.service";
import {RouteConstants} from "../constants/routes.constants";

@Injectable({providedIn:'root'})
export class HeaderService extends BaseService{
    constructor(
        private router:Router,
        private http:HttpClient
    ){
        super();
    }

    getHeaderdata(){
        return this.http.get<Config>(`${this.getBaseUrl()}${RouteConstants.headerData}`,this.getConfig());
    }
    getProfileHeaderdata(){
        return this.http.get<Config>(`${this.getBaseUrl()}${RouteConstants.profileHeaderData}`,this.getConfig());
    }
}