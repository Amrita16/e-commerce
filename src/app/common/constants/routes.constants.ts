export class RouteConstants{
    public static signup = "signUp";
    public static login = "login";
    public static headerData = "headerData";
    public static profileHeaderData = "profileHeaderData";
}