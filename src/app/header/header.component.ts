import { Component, OnInit, } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService } from '../common/services/auth.service';
import { HeaderService } from '../common/services/header.service';
declare var $:any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showProfileIcon: boolean = true;
  headerData: any;
  profileHeaderData: any;
  isLoggedIn: boolean = false;

  constructor(private router:Router,private headerService:HeaderService,private authService:AuthService) { }

  ngOnInit(){
    $('.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
      }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
    });
    let token = this.authService.getAuthToken();
    if(token) this.isLoggedIn = true;
    this.getHeaderData();
    this.getProfileHeaderdata();
  }

  toggleProfileIcon(){
    this.showProfileIcon = false;
  }

  routeTo(){
    this.router.navigate(['wishlist'])
  }

  getHeaderData(){
    this.headerService.getHeaderdata().subscribe(response=>{
      if(response.status){
        this.headerData = response.data;
      }
    })
  }

  getProfileHeaderdata(){
    this.headerService.getProfileHeaderdata().subscribe(response=>{
      if(response.status){
        this.profileHeaderData = response.data[0];
      }
    })
  }
}
