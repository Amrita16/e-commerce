import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { ListingComponent } from './modules/listing/listing.component';
import { ProductListComponent } from './common/product-list/product-list.component';
import { FilterGridComponent } from './common/filter-grid/filter-grid.component';
import { WishlistComponent } from './modules/wishlist/wishlist.component';
import { ProductItemComponent } from './modules/product-item/product-item.component';
import { CartComponent } from './modules/cart/cart.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { BaseInterceptor } from './common/services/base-interceptor.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ListingComponent,
    ProductListComponent,
    FilterGridComponent,
    WishlistComponent,
    ProductItemComponent,
    CartComponent,
    SignUpComponent,
    ForgotPwdComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:BaseInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
