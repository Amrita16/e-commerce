import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ISignUp } from '../common/interface';
import { AuthService } from '../common/services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit,OnDestroy {
  signUpForm:FormGroup;
  unsubscribe = new Subject<void>();
  constructor(private loginService:AuthService,private router:Router,private fb:FormBuilder) { }

  ngOnInit(): void {
    $('[data-toggle="tooltip"]').tooltip();
    this.createForm();
  }
  createForm(){
    this.signUpForm = this.fb.group({
      firstname:['',Validators.required],
      lastname:['',Validators.required],
      email:['',(Validators.required,Validators.email)],
      password:['',Validators.required]
    });
  }
  signup(){
    if(this.signUpForm.valid){
      let formData:ISignUp = this.signUpForm.value;
        this.loginService.signUp(formData).pipe(takeUntil(this.unsubscribe)).subscribe(response=>{
          if(response.status){
            this.router.navigate(['/login']);
          }else{
            console.log(response.error);
          }
        });
    }else{
      console.log(`Please enter all the fields.`);
    }
  }
  ngOnDestroy(){
    if(this.unsubscribe){
      this.unsubscribe.next();
      this.unsubscribe.complete();
    }
  }
}


