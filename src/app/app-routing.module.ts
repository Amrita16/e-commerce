import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './common/services/auth-guard.service';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { CartComponent } from './modules/cart/cart.component';
import { ListingComponent } from './modules/listing/listing.component';
import { ProductItemComponent } from './modules/product-item/product-item.component';
import { WishlistComponent } from './modules/wishlist/wishlist.component';
import { SignUpComponent } from './sign-up/sign-up.component';


const routes: Routes = [
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'signup',
    component:SignUpComponent
  },
  {
    path:'resetPassword',
    component:ForgotPwdComponent
  },
  {
    path:'products',
    component:ListingComponent
  },
  {
    path:'wishlist',
    component:WishlistComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'product',
    component:ProductItemComponent
  },
  {
    path:'cart',
    component:CartComponent,
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
