const express = require("express");
const mongoose = require("mongoose");
const app = express();
const userRoutes = require("./routes/user.routes");
const headerRoutes = require("./routes/header.routes");

mongoose.connect('mongodb+srv://amy:Rasengen_2410@cluster0.rreyf.mongodb.net/ecommerce?retryWrites=true&w=majority')
.then(()=>{
    console.log('Connected to database.')
})
.catch(()=>{
    console.log('Connection Failed.')
});

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers",
    "*");
    res.setHeader("Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS, PUT"); 
    next(); 
});
app.use('/',headerRoutes);
app.use('/',userRoutes);
// password: GJlh3sITnFopFm4n
module.exports = app;