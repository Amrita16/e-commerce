const express = require("express");
const route = express.Router();
const headerRoute = require("../controllers/header.controller");

route.get('/headerData',headerRoute.getHeaderData);
route.get('/profileHeaderData',headerRoute.getProfileHeaderData);

module.exports = route;