const express = require("express");
const router = express.Router();
const userRoutes = require("../controllers/user.controller");
const checkToken = require("../middleware/check-authToken");
router.post("/signUp", userRoutes.signup);
router.post("/login",userRoutes.login);

module.exports = router;