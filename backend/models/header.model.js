const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let headerSchema = new Schema({
    category:{type:String},
    sections:[{
        sectionTitle:{type:String},
        subSections:[{
            title:{type:String},
            code:{type:Number}
        }]
    }]
});

module.exports = mongoose.model('Header',headerSchema);