const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let profileSchema = new Schema({
    profileSection1:[{type:String}],
    profileSection2:[{type:String}],
    profileSection3:[{type:String}],
});

module.exports = mongoose.model('Profile',profileSchema);