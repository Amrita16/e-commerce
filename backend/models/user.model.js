const mongoose = require("mongoose");
// uniqueValidator makes sure that the email used is unique.
const uniqueValidator = require("mongoose-unique-validator");
const Schema = mongoose.Schema;

let userSchema = new Schema({
    firstname: { type: String, required: true},
    lastname: { type: String, required: true},
    email: { type: String, required: true, unique: true},
    password: { type: String, required: true}
});
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User',userSchema);