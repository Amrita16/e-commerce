const jwt = require("jsonwebtoken");
const config = require("../common/config");
module.exports = (req,res,next)=>{
    try{
        let token = req.header['X-Auth-Token'];
        jwt.verify(token,config.secretKey);
        next();
    }catch(err){
         res.status(401).json({
            status:false,
            error:"Authentication Failed."
        });
    }
}