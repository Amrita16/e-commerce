const bcrypt = require("bcrypt");
const User = require("../models/user.model");
const jwt = require("jsonwebtoken");
const config = require("../common/config");
const messages = {
    user_added:"User added successfully.",
    user_not_found:"Auth failed.",
    user_found:"User logged in successfully."
}


exports.signup = (req,res) =>{
    bcrypt.hash(req.body.password, 10).then(hash=>{
        const user = new User({
            firstname: req.body.firstname.toLowerCase(),
            lastname: req.body.lastname.toLowerCase(),
            email: req.body.email.toLowerCase(),
            password: hash
        });
        user.save().then(result => {
            res.status(201).json({
                status: true,
                message: messages.user_added,
                data: result
            });
        }).catch(err=>{
            res.status(500).json({
                status:false,
                error:err
            })
        });
    });
};

exports.login = (req,res)=>{
    let fetchedUser;
    User.findOne({ email:req.body.email }).then(user=>{
        if(!user){
            return res.status(401).json({
                status:false,
                message:messages.user_not_found
            });
        };
        fetchedUser = user;
        bcrypt.compare(req.body.password,user.password).then(result=>{
            if(!result){
                return res.status(401).json({
                    status:false,
                    message:messages.user_not_found
                });
            }
            const token = jwt.sign({email: fetchedUser.email, userId: fetchedUser._id},config.secretKey,{expiresIn: "4h"});
            res.status(200).json({
                status:true,
                message:messages.user_found,
                token:token,
                expiresIn: 7200
            })
        }).catch(err=>{
            return res.status(401).json({
                status:false,
                message:err
            });
        });
    });
};