const Header = require("../models/header.model");
const Profile = require("../models/profile-header.model");
const Constants = require("../common/constants");

exports.getHeaderData = (req,res)=>{
    Header.find().then(data=>{
       return res.status(200).json({
            status:true,
            message: Constants.data_found,
            data: data
        });
    }).catch(err=>{
        res.status(404).json({
            status:false,
            error:err
        });
    });
};

exports.getProfileHeaderData = (req,res)=>{
    Profile.find().then(result=>{
       return res.status(200).json({
            status:true,
            message: Constants.data_found,
            data: result
        });
    }).catch(err=>{
        res.status(404).json({
            status:false,
            error:err
        });
    });
};